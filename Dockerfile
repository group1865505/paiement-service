FROM node:16.15.0-alpine

ENV NODE_ENV=production
WORKDIR /app

COPY ["package.json", "package-lock.json", "./"]
RUN npm install --${NODE_ENV}

COPY . .

EXPOSE 5001


CMD ["node", "server.js"]