const express = require('express');
const router = express.Router();
const axios = require('axios');

router.post('/', async (req, res) => {
   
    const { commandeId } = req.body;
  
    try {
      

      // Appeler le microservice commande pour mettre à jour le statut de la commande
      const response = await axios.put(`http://localhost:5000/api/commandes/${commandeId}`); 
  
      // Renvoyer la réponse au client avec le paiement enregistré
      res.status(201).json({message : 'Paiement Accepté'});
    } catch (error) {
      console.error(error);
      res.status(500).json({message: 'Le paiement n\'a été enregistré'})
    }
  });

module.exports = router ;