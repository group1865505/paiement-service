const express = require('express');
const bodyParser = require('body-parser');
const PaiementController = require('./routes/api/PaiementController');
const app = express();
//const cors = require('cors');

const cors = require('cors');
const corsOptions = {
  origin: '*' ,
  credentials: true,
  'allowedHeaders': ['sessionId', 'Content-type'],
  'exposedHeaders': ['sessionId'],
  'methods': 'GET, HEAD, PUT, PATCH, POST, DELETE',
  'preflightContinue': false
}
app.use(cors(corsOptions));

//app.use(cors());

app.use(bodyParser.json());


app.use('/api/paiement', PaiementController);

const port = process.env.PORT || 5001 ;

app.listen(port , ()=> console.log(`Le Microservice Paiement est démaré sur le port ${port}`));
